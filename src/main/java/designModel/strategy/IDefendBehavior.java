package designModel.strategy;

public interface IDefendBehavior {
    void defend();
}
