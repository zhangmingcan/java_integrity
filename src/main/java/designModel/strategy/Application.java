package designModel.strategy;

public class Application {
    public static void main(String[] args) {
        Role roleA = new RoleA("A");

        roleA.setAttackBehavior(new AttackTS())
                .setDefendBehavior(new DefendTS());
        System.out.println(roleA.name + ":");
        roleA.attack();
        roleA.defend();
    }
}
