package designModel.strategy;

public interface IAttackBehavior {
    void attack();
}
