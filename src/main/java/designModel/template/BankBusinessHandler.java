package designModel.template;

import java.math.BigDecimal;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class BankBusinessHandler {
    public void save(BigDecimal amount) {
        execute(()->"Number-00"+new Random().nextInt(),a -> System.out.println("save " + amount));
    }

    public void saveVip(BigDecimal amount) {
        execute(()->"vipNumber-00"+new Random().nextInt(),a -> System.out.println("save " + amount));
    }

    public void saveReservation(BigDecimal amount) {
        execute(() -> "reservationNumber-00" + new Random().nextInt(), a -> System.out.println("save " + amount));
    }


    protected void execute(Supplier<String> supplier,Consumer<BigDecimal> consumer) {
        String type = supplier.get();
        System.out.println(type);

        if(type.startsWith("vip")){
            System.out.println("Assign To Vip Counter");
        }else if(type.startsWith("reserve")){
            System.out.println("Assign To Exclusive Customer Manager");
        }else {
            System.out.println("Assign To Usual Manager");
        }

        consumer.accept(null);
        judge();
    }

    private void getNumber() {
        System.out.println("number-00" + new Random().nextInt());

    }

    private void judge() {
        System.out.println("give a praised");
    }

}
