package designModel.template;

import java.util.Random;

public abstract class AbstractBusinessHandler {
    /**
     * 模版方法
     */
    public final void execute(){
        getNumber();
        handle();
        judge();
    }

    /**
     * 取号
     * @return
     */
    private void getNumber(){
        System.out.println("number-00-" + new Random().nextInt());
    }

    /**
     * 办理业务
     */
    public abstract void handle(); //抽象的办理业务方法，由子类实现

    /**
     * 评价
     */
    private void judge(){
        System.out.println("give a praised");

    }
}
