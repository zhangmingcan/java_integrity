# 模版模式

重点在于关键方法的模版编排，可以为接口方法进行实现（区别于策略模式）

## JDK8之前
需要准备一个抽象类，将部分逻辑以具体方法以及具体构造函数的形式实现，然后声明一些抽象方法来让子类实现剩余的逻辑。不同的子类可以以不同的方式实现这些抽象方法，从而对剩余的逻辑有不同的实现。

`AbstractBusinessHandler`和`SaveMoneyHandler`采用抽象类继承实现

```java
public abstract class AbstractBusinessHandler {
    /**
     * 模版方法
     */
    public final void execute(){
        getNumber();
        handle();
        judge();
    }

    /**
     * 取号
     * @return
     */
    private void getNumber(){
        System.out.println("number-00-" + new Random().nextInt());
    }

    /**
     * 办理业务
     */
    public abstract void handle(); //抽象的办理业务方法，由子类实现

    /**
     * 评价
     */
    private void judge(){
        System.out.println("give a praised");

    }
}
```

```java
public class SaveMoneyHandler extends AbstractBusinessHandler{
    @Override
    public void handle() {
        System.out.println("save 1000");
    }
}
```



## JDK8之后

  支持函数式编程

```java
- Consumer
- Supplier
- Predicate
- Function 
```

  对于模版主要使用Supplier和Consumer两个接口



# 参考

https://mp.weixin.qq.com/s/zpScSCktFpnSWHWIQem2jg



