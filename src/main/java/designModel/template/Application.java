package designModel.template;

import java.math.BigDecimal;

public class Application {
    public static void main(String[] args) {
        System.out.println("==== Abstract class begin ====");
        SaveMoneyHandler saveMoneyHandler = new SaveMoneyHandler();
        saveMoneyHandler.execute();
        System.out.println("==== Abstract class end ====");
        System.out.println();

        System.out.println("==== Consumer interface begin ====");
        BankBusinessHandler bankBusinessHandler = new BankBusinessHandler();
        bankBusinessHandler.save(new BigDecimal(1000));
        System.out.println("==== Consumer interface end ====");
    }
}
