package designModel.decorate;

public class RingEquip implements IEquip{
    @Override
    public int calculateAttack() {
        return 5;
    }

    @Override
    public String description() {
        return "重楼戒";
    }
}
