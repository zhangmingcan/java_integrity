package designModel.decorate;

public class Application {
    public static void main(String[] args) {
        System.out.println("一个镶嵌2颗蓝宝石，1颗黄宝石的武器");
        IEquip equip = new BlueGemDecorator(new BlueGemDecorator(new YellowGemDecorator(new ArmEquip())));
        System.out.println("攻击力：" + equip.calculateAttack());
        System.out.println("描述：" + equip.description());
    }
}
