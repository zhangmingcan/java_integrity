package designModel.decorate;

public interface IEquip {

    public int calculateAttack();

    public String description();

}
