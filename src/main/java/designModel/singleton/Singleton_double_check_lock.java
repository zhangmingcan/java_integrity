package designModel.singleton;

public class Singleton_double_check_lock {
    // volatile 保证初始化过程不发生重排序，导致不会有半创建完成实例
    private volatile static Singleton_double_check_lock instance;

    private Singleton_double_check_lock(){}

    public static Singleton_double_check_lock getInstance(){
        if(instance == null){
            synchronized (Singleton_double_check_lock.class){
                if(instance == null){
                    instance = new Singleton_double_check_lock();
                }
            }
        }
        return instance;
    }
}
