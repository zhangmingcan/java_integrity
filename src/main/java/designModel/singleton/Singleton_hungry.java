package designModel.singleton;

public class Singleton_hungry {
    private static Singleton_hungry instance = new Singleton_hungry();

    private Singleton_hungry(){}

    public static Singleton_hungry getInstance() {
        return instance;
    }
}
