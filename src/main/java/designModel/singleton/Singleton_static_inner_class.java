package designModel.singleton;

public class Singleton_static_inner_class {
    private static class SingletonHolder{
        private static final Singleton_static_inner_class INSTANCE = new Singleton_static_inner_class();
    }
    private Singleton_static_inner_class(){}

    public static final Singleton_static_inner_class getInstance(){
        return SingletonHolder.INSTANCE;
    }
}
