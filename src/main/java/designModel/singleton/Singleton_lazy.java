package designModel.singleton;

public class Singleton_lazy {
    private static Singleton_lazy instance;

    private Singleton_lazy(){}

    // public static synchronized Singleton_lazy getInstance()
    public static Singleton_lazy getInstance() {
        if(instance == null){
            instance = new Singleton_lazy();
        }
        return instance;
    }
}
