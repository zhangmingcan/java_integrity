package designModel.state;

import sun.rmi.runtime.Log;

public class WinnerState implements State{

    private StatedVendingMachine machine;

    public WinnerState(StatedVendingMachine machine) {
        this.machine = machine;
    }

    @Override
    public void insertMoney() {
        throw new IllegalStateException("非法操作!");
    }

    @Override
    public void backMoney() {
        throw new IllegalStateException("非法操作!");
    }

    @Override
    public void turnCrank() {
        throw new IllegalStateException("非法操作!");
    }

    @Override
    public void dispense() {
        System.out.println("WinnerState ---您中奖了, 恭喜您,将获得2件商品!");
        machine.dispense();
        if (machine.getCount() == 0) {
            System.out.println("WinnerState ---商品已售罄,中奖失效...");
            machine.setState(machine.getSoldOutState());
        } else {
            machine.dispense();
            if (machine.getCount() > 0) {
                machine.setState(machine.getNoMoneyState());
            } else {
                machine.setState(machine.getSoldOutState());
            }
        }
    }
}
