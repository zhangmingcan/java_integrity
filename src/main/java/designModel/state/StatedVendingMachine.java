package designModel.state;

import sun.rmi.runtime.Log;

public class StatedVendingMachine {

    private int count = 0;

    private State currentState;
    private State noMoneyState;
    private State hasMoneyState;
    private State soldState;
    private State soldOutState;
    private State winnerState;

    public StatedVendingMachine(int count) {
        noMoneyState = new NoMoneyState(this);
        hasMoneyState = new HasMoneyState(this);
        soldState = new SoldState(this);
        soldOutState = new SoldOutState(this);
        winnerState = new WinnerState(this);

        if (count > 0) {
            this.count = count;
            currentState = noMoneyState;
        }
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }

    public State getCurrentState() {
        return currentState;
    }

    /**
     * 放钱
     */
    public void insertMoney() {
        currentState.insertMoney();
    }

    /**
     * 退钱
     */
    public void backMoney() {
        currentState.backMoney();
    }

    /**
     * 转动曲柄
     */
    public void turnCrank() {
        currentState.turnCrank();
        if (currentState == soldState || currentState == winnerState) {
            currentState.dispense();//两种情况会出货
        }
    }

    /**
     * 出商品
     */
    public void dispense() {
        if (count > 0) {
            System.out.println("StatedVendingMachine ---发出一件商品");
            count--;
        }
    }

    public void setState(State state) {
        this.currentState = state;
    }

    public State getNoMoneyState() {
        return noMoneyState;
    }

    public State getHasMoneyState() {
        return hasMoneyState;
    }

    public State getSoldState() {
        return soldState;
    }

    public State getSoldOutState() {
        return soldOutState;
    }

    public State getWinnerState() {
        return winnerState;
    }

    public int getCount() {
        return count;
    }
}
