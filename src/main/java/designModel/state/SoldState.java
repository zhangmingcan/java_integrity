package designModel.state;

import sun.rmi.runtime.Log;

public class SoldState implements State{

    private StatedVendingMachine machine;

    public SoldState(StatedVendingMachine machine) {
        this.machine = machine;
    }

    @Override
    public void insertMoney() {
        System.out.println("SoldState ---正在出货,请勿投币");
    }

    @Override
    public void backMoney() {
        System.out.println("SoldState ---正在出货,没有可退的钱");
    }

    @Override
    public void turnCrank() {
        System.out.println("SoldState ---正在出货,请勿重复摇动手柄");
    }

    @Override
    public void dispense() {
        // 减数量
        machine.dispense();

        // 状态转移
        if (machine.getCount() > 0) {
            machine.setState(machine.getNoMoneyState());
        } else {
            System.out.println("SoldState ---商品已经售罄");
            machine.setState(machine.getSoldOutState());
        }
    }
}
