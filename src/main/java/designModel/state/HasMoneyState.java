package designModel.state;

import org.w3c.dom.ls.LSOutput;
import sun.rmi.runtime.Log;

import java.util.Random;

public class HasMoneyState implements State {

    private StatedVendingMachine machine;
    Random random = new Random();

    public HasMoneyState(StatedVendingMachine machine) {
        this.machine = machine;
    }

    @Override
    public void insertMoney() {
        System.out.println("HasMoneyState ---您已经投过币了,不用再投了");
    }

    @Override
    public void backMoney() {
        System.out.println("HasMoneyState ---退币成功");
        machine.setState(machine.getNoMoneyState());
    }

    @Override
    public void turnCrank() {
        System.out.println("HasMoneyState ---你转动了手柄...");

        if (machine.getCount() > 0) {
            int winner = random.nextInt(10);
            if (winner == 0) {
                machine.setState(machine.getWinnerState());
            } else {
                machine.setState(machine.getSoldState());
            }
        }
    }

    @Override
    public void dispense() {
        System.out.println("HasMoneyState ---只有在售出商品状态时才能出货!");
        throw new IllegalStateException("非法状态!");
    }
}
